import React from "react";

const DataDisplayComponent = ({ data }) => {
  const renderData = Object.entries(data).map(([key, value]) => (
    <div key={key} className="data-item">
      <strong>{key}</strong> {value}
    </div>
  ));

  return <div className="flex-container">{renderData}</div>;
};

export default DataDisplayComponent;
