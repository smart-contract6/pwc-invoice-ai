import json

with open("input.json", "r") as json_file:
    # Parse the JSON data from the file
    data = json.load(json_file)

# print(data["data"]["tabular_data"])  # Output: John

def generate_table_json(data):
    tables = {}

    for table_data in data:
        table = {'header': [], 'data': []}

        for key, value in table_data.items():
            row, col = value[0]
            cell_value = value[1]

            while len(table['data']) <= row:
                table['data'].append([])
            while len(table['header']) <= col:
                table['header'].append(None)

            if table['header'][col] is None:
                table['header'][col] = cell_value
            else:
                table['data'][row].append(cell_value)

        tables[f'table{len(tables) + 1}'] = table

    return {'tables': tables}


json_output = generate_table_json(data["data"]["tabular_data"])
# print(json.dumps(json_output, indent=4))

with open('output.json', 'w') as json_file:
    json.dump(json_output, json_file, indent=4)

print("JSON data has been saved to 'output.json'")



