import React, { useState } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

 const data = {
        "header": [
          "Annexure-1",
          "Program",
          "Producer",
          "",
          "",
          "",
          "No of Spots",
          "Net Cost"
        ],
        "data": [
          [],
          [
            "Channel",
            null,
            null,
            "Dates",
            "Spot Duration",
            "Net Spot Rate\nPer 10 sec"
          ],
          [
            "MNX",
            "FIXED",
            "MATRIX PUBLICITIES AND MEDIA INDIA PVT LIMITED",
            "16(1),19(2),20(4),21(5),22 (4),23(5),26(6),27(6),28 (7),29(6),30(4)",
            "30",
            "1,146.07",
            "50",
            "1,71,910.50"
          ],
          [
            "MNX",
            "RODP (09.00 - 17.00)",
            "MATRIX PUBLICITIES AND MEDIA INDIA PVT LIMITED",
            "17(7),18(7),24(12),25(18),31 (6)",
            "30",
            "611.24",
            "50",
            "91,686.00"
          ],
          [
            "|MNX",
            "RODP (09.00 - 18.00)",
            "MATRIX PUBLICITIES AND MEDIA INDIA PVT LIMITED",
            "16(2),19(1),20(6),21(5),22 (4),23(5),26(6),27(6),28 (5),29(4),30(4)",
            "30",
            "152.81",
            "48",
            "22,004.64"
          ],
        ]
        };
    

        
        const Table = () => {
          const [columns, setColumns] = useState(data.header);
          const [tableData, setTableData] = useState(data.data);
        
          const onDragEnd = (result) => {
            if (!result.destination) {
              return;
            }
        
            const reorderedColumns = Array.from(columns);
            const [removed] = reorderedColumns.splice(result.source.index, 1);
            reorderedColumns.splice(result.destination.index, 0, removed);
        
            const reorderedData = tableData.map((row) => {
              const newRow = Array.from(row);
              const [removedData] = newRow.splice(result.source.index, 1);
              newRow.splice(result.destination.index, 0, removedData);
              return newRow;
            });
        
            setColumns(reorderedColumns);
            setTableData(reorderedData);
          };
        
          return (
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="columns" direction="horizontal">
                {(provided) => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    <table>
                      <thead>
                        <tr>
                          {columns.map((header, index) => (
                            <Draggable key={header} draggableId={header} index={index}>
                              {(provided) => (
                                <th
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  ref={provided.innerRef}
                                >
                                  {header}
                                </th>
                              )}
                            </Draggable>
                          ))}
                        </tr>
                      </thead>
                      <tbody>
                        {tableData.map((row, index) => (
                          <tr key={index}>
                            {row.map((cell, cellIndex) => (
                              <td key={cellIndex}>{cell}</td>
                            ))}
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          );
        };
        
        export default Table;
        