import React from "react";
import { saveAs } from "file-saver";

const JsonToTables = ({ data }) => {
  function removeEmptyArrays(inputArray) {
    return inputArray.filter((item) =>
      Array.isArray(item) ? item.length > 0 : true
    );
  }

  function removeDuplicates(inputArray) {
    return Array.from(new Set(inputArray));
  }

  function convertToCSV(data) {
    const csvRows = [];
    for (const row of data) {
      csvRows.push(row.join(","));
    }
    return csvRows.join("\n");
  }

  const handleExport = (data, header) => {
    const headerArray = [header];

    const allData = removeEmptyArrays([...headerArray, ...data]);

    console.log(allData);
    // return;

    const csvData = convertToCSV(allData);
    const blob = new Blob([csvData], { type: "text/csv;charset=utf-8" });
    saveAs(blob, "data.csv");
  };

  return (
    <div>
      {Object.keys(data.tables).map((tableKey, index) => (
        <div key={index} className="table-container">
          <h2>Table {index + 1}</h2>
          <button
            onClick={() =>
              handleExport(
                data.tables[tableKey].data,
                data.tables[tableKey].header
              )
            }
          >
            Export to CSV
          </button>
          <table className="table">
            <thead>
              <tr>
                {data.tables[tableKey].header.map((header, headerIndex) => (
                  <th key={headerIndex}>{header}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data.tables[tableKey].data.map((row, rowIndex) => (
                <tr key={rowIndex}>
                  {row.map((cell, cellIndex) => (
                    <td key={cellIndex}>{cell}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      ))}
    </div>
  );
};

export default JsonToTables;
