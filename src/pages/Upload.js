import React, { useState } from "react";
import Home from "./Home";

import KeyValues from "../components/KeyValues";

const FileUploadComponent = () => {
  const [file, setFile] = useState(null);
  const [loading, setLoading] = useState(false);
  const [apiResponse, setApiResponse] = useState(null);
  const [responseData, setApiResponseData] = useState(null);

  const handleFileChange = (event) => {
    const selectedFile = event.target.files[0];
    setFile(selectedFile);
  };

  const generateTableJson = (data) => {
    const generatedTables = {};

    data.forEach((tableData, index) => {
      const table = { header: [], data: [] };

      Object.entries(tableData).forEach(([key, value]) => {
        const [row, col] = value[0];
        const cellValue = value[1];

        while (table.data.length <= row) {
          table.data.push([]);
        }
        while (table.header.length <= col) {
          table.header.push(null);
        }

        if (table.header[col] === null) {
          table.header[col] = cellValue;
        } else {
          table.data[row][col] = cellValue;
        }
      });

      generatedTables[`table${index + 1}`] = table;
    });

    return { tables: generatedTables };
  };

  const handleUpload = () => {
    if (file) {
      var formdata = new FormData();
      formdata.append("file", file);

      var requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };
      setLoading(true);
      fetch(
        "https://table-extracter-backend.azurewebsites.net/analyze",
        requestOptions
      )
        .then((response) => response.json()) // Parse response as JSON
        .then((result) => {
          setApiResponseData(result);
          setApiResponse(generateTableJson(result.data.tabular_data));
          setLoading(false);
        }) // Store API response in state
        .catch((error) => console.log("error", error));
    } else {
      console.log("No file selected!");
    }
  };

  console.log("responseData", responseData);
  return (
    <div>
      <input type="file" onChange={handleFileChange} />
      <button onClick={handleUpload}>Upload</button>

      {loading && <div className="loader"></div>}
      {responseData && <KeyValues data={responseData?.data?.key_value} />}
      {apiResponse && (
        <div className="json-view">
          <h2>API Response:</h2>

          <Home data={apiResponse} />
          <pre>{JSON.stringify(apiResponse, null, 2)}</pre>
        </div>
      )}
    </div>
  );
};

export default FileUploadComponent;
